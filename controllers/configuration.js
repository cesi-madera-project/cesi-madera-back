const Model = require("../models/Model");
const Range = require("../models/Range");
const FamilyComponent = require("../models/FamilyComponent");
const Component = require("../models/Component");
const Provider = require("../models/Provider");
const Frame = require("../models/Frame");
const Setting = require("../models/Setting");

exports.getAllModel = (req, res, next) => {
    Model.find({})
        .then(models => res.status(200).json(models))
        .catch(error => res.status(400).json({
            error
        }));
};
exports.createModel = (req, res, next) => {
    let newModel = new Model(req.body);
    newModel.save((err,model) => {
	if(err) {
		res.send(err);
	}
	else {
    		res.json({message: "modele created!", model})
	}
    })
};
exports.getOneModel = (req, res, next) => {
    res.end('VOICI U modele');
};
exports.modifyModel = (req, res, next) => {
    res.end('MODIFIER UN modele');
};
exports.deleteModel = (req, res, next) => {
    res.end('SUPPRIME UN modele');
}

exports.getAllRange = (req, res, next) => {
    Range.find({})
        .then(ranges => res.status(200).json(ranges))
        .catch(error => res.status(400).json({
            error
        }));
};
exports.createRange = (req, res, next) => {
    res.end('CRER UNe gamme');
};
exports.getOneRange = (req, res, next) => {
    res.end('VOICI UNe gamme');
};
exports.modifyRange = (req, res, next) => {
    res.end('MODIFIER UNe gamme');
};
exports.deleteRange = (req, res, next) => {
    res.end('SUPPRIME UNe gamme');
}

exports.getAllFamilyComponent = (req, res, next) => {
    FamilyComponent.find({})
        .then(familycomponents => res.status(200).json(familycomponents))
        .catch(error => res.status(400).json({
            error
        }));
};
exports.createFamilyComponent = (req, res, next) => {
    res.end('CRER UNe famille de composant');
};
exports.getOneFamilyComponent = (req, res, next) => {
    res.end('VOICI UNe famille de composant');
};
exports.modifyFamilyComponent = (req, res, next) => {
    res.end('MODIFIER UNe famille de composant');
};
exports.deleteFamilyComponent = (req, res, next) => {
    res.end('SUPPRIME UNe famille de composant');
}

exports.getAllComponent = (req, res, next) => {
    Component.find({})
        .then(components => res.status(200).json(components))
        .catch(error => res.status(400).json({
            error
        }));
};
exports.createComponent = (req, res, next) => {
    res.end('CRER UN composant');
};
exports.getOneComponent = (req, res, next) => {
    res.end('VOICI UN composant');
};
exports.modifyComponent = (req, res, next) => {
    res.end('MODIFIER UN composant');
};
exports.deleteComponent = (req, res, next) => {
    res.end('SUPPRIME UN composant');
}

exports.getAllProvider = (req, res, next) => {
    Provider.find({})
        .then(providers => res.status(200).json(providers))
        .catch(error => res.status(400).json({
            error
        }));
};
exports.createProvider = (req, res, next) => {
    res.end('CRER UN fournisseur');
};
exports.getOneProvider = (req, res, next) => {
    res.end('VOICI UN fournisseur');
};
exports.modifyProvider = (req, res, next) => {
    res.end('MODIFIER UN fournisseur');
};
exports.deleteProvider = (req, res, next) => {
    res.end('SUPPRIME UN fournisseur');
}

exports.getAllFrame = (req, res, next) => {
    Frame.find({})
        .then(frames => res.status(200).json(frames))
        .catch(error => res.status(400).json({
            error
        }));
};
exports.createFrame = (req, res, next) => {
    res.end('CRER UNe huisserie');
};
exports.getOneFrame = (req, res, next) => {
    res.end('VOICI UNe huisserie');
};
exports.modifyFrame = (req, res, next) => {
    res.end('MODIFIER UNe huisserie');
};
exports.deleteFrame = (req, res, next) => {
    res.end('SUPPRIME UNe huisserie');
}

exports.getAllSetting = (req, res, next) => {
    Setting.find({})
        .then(settings => res.status(200).json(settings))
        .catch(error => res.status(400).json({
            error
        }));
};
exports.createSetting = (req, res, next) => {
    res.end('CRER UN paramètre');
};
exports.getOneSetting = (req, res, next) => {
    res.end('VOICI UN paramètre');
};
exports.modifySetting = (req, res, next) => {
    res.end('MODIFIER UN paramètre');
};
exports.deleteSetting = (req, res, next) => {
    res.end('SUPPRIME UN paramètre');
}
