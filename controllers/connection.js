const User = require("../models/User");
const bcrypt = require('bcrypt');
const jwtUtils = require('../utils/jwt.utils');

/**
 *  async (req, res, next) => {
    var logintest = await test(queryLogin);
    if(logintest.response === 'ERROR')
 * @param {*} queryLogin 

 */
const test = (queryLogin) => {
    return new Promise((resolve) => {

        User.findOne({ login: queryLogin }, (err, login) => {
            if (err) {
                resolve({
                    response: 'ERROR',
                    error: err
                })
            } else {
                resolve({
                    response: 'SUCCESS',
                    login: login
                })
            }
        });
    });
}
const getLoginAcess = (req, res) => {
    //Déclaration de variables et récupération
    var queryLogin = req.body.login;
    var queryPassword = req.body.password;

    //Recherche de l'utilisateur en BDD
    User.findOne({ login: queryLogin }, (err, login) => {
        var result = login;
        if (err) {
            return res.status(400).json({ 'error': err });
        } else {
            if (result) {
                //comparaison du mot de passe
                bcrypt.compare(queryPassword, result.password, function (err, resul) {
                    if (!resul) {
                        return res.status(403).json({ 'error': 'Mots de passe invalide' });
                    } else {
                        //Création du TOKEN
                        const jwtTest = jwtUtils.generateTokenForUser(result);
                        return res.status(200).json({
                            'userId': result._id,
                            'login': result.login,
                            'firstName': result.firstname,
                            'token': jwtUtils.generateTokenForUser(result),
                            'session': req.session,
                            'sessionId': req.session.id
                        });
                    }
                });
            } else {
                return res.status(400).json({ 'error': 'Utilisateur inexistant' });
            }
        }
    });
}

module.exports = {
    getLoginAcess: getLoginAcess
}
