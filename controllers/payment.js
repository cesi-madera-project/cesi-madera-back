const Project = require("../models/Project");
const Customer = require("../models/Customer");
const Payment = require("../models/Payment");

exports.getAllProjectPayment = (req, res, next) => {
    Project.find({ payment: { $exists: true }}).populate('customer').populate('payment')
        .then(projects => res.status(200).json(projects))
        .catch(error => res.status(400).json({
            error
        }));
};
exports.createProjectPayment = (req, res, next) => {
    res.end('CRER UN paiement de projet');
};
exports.getOneProjectPayment = (req, res, next) => {
    res.end('VOICI UN paiement de projet');
};
exports.modifyProjectPayment = (req, res, next) => {
    res.end('MODIFIER UN paiement de projet');
};
exports.deleteProjectPayment = (req, res, next) => {
    res.end('SUPPRIME UN paiement de projet');
}

exports.getAllPayment = (req, res, next) => {
    Payment.find({})
        .then(payments => res.status(200).json(payments))
        .catch(error => res.status(400).json({
            error
        }));
};
exports.createPayment = (req, res, next) => {
    res.end('CRER UN paiement');
};
exports.getOnePayment = (req, res, next) => {
    res.end('VOICI UN paiement');
};
exports.modifyPayment = (req, res, next) => {
    res.end('MODIFIER UN paiement');
};
exports.deletePayment = (req, res, next) => {
    res.end('SUPPRIME UN paiement');
}