const Project = require("../models/Project");
const State = require("../models/State");
const Customer = require("../models/Customer");
const Payment = require("../models/Payment");
const Product = require("../models/Product");
const ProjectProduct = require("../models/ProjectProduct");
const Module = require("../models/Module");
const ProjectModule = require("../models/ProjectModule");
const QuoteComponent = require("../models/QuoteComponent");
const ProjectComponent = require("../models/ProjectComponent");
const { where } = require("../models/Customer");
const { isValidObjectId } = require("mongoose");
const fs = require("fs");
const PDFDocument = require("pdfkit");

const getAllUserQuotesList = (req,res) => {
    //déclarations variables
    var userId = req.body.idUser;

    Project.find({user: userId}).populate('state').populate('customer')
    .then(projects => res.status(200).json(projects))
    .catch(error => res.status(500).json({ 'error' : error }));
};
const getAllUserProgressQuotesList = (req, res) => {
    //déclarations variables
    var userId = req.body.idUser;

    Project.find({user: userId, state : '5f1024c37b23f59e1bc924bc'}).populate('state').populate('customer')
    .then(projects => res.status(200).json(projects))
    .catch(error => res.status(500).json({ 'error' : error }));
};
const getAllUserValidateQuotesList = (req, res) => {
    //déclarations variables
    var userId = req.body.idUser;
    console.log(userId);
    
    if (userId == null || userId == ''){
        return res.status(500).json({'error': 'userId vide ou null'});
    }
    Project.find({user: userId}).exists('payment').populate('state').populate('customer')
    .then(projects => res.status(200).json(projects))
    .catch(error => res.status(500).json({ 'error' : error }));
};
const getAllUserQuotesListByPhoneNumber = (req,res) => {
    console.log('getAllUserQuotesListByPhoneNumber');
    var phone_number = req.body.cli_phonenumber;
    console.log(phone_number);
    Customer.findOne({phoneNumber: phone_number}, (err, Customer) => {
        console.log(Customer);
        console.log(Customer._id);
        if(err){
            return err;
        }else{
            customerId = Customer._id;
            console.log(customerId);
            Project.find({customer: customerId}).populate('state').populate('customer')
            .then(projects => res.status(200).json(projects))
            .catch(error => res.status(500).json({ 'error' : error }));
        }
    })
    /*.then(Project => res.status(200).json(Project))*/
    .catch(error => res.status(500).json({ 'error' : error }));
}
const getResearchCustomer = (req, res) => {
    //déclarations variables
    var phonenumber = req.body.cli_phonenumber;
    var name = req.body.cli_name;
    if (phonenumber != " "){
        Customer.findOne({phoneNumber: phonenumber}, (err, Customer) => {
            if(err){
                return err;
            }else{
                return Customer;
            }
        })
        .then(Customer => res.status(200).json(Customer))
        .catch(error => res.status(500).json({ 'error' : error }));
    }else{
        Customer.findOne({_id: name}, (err, Customer) => {
            if(err){
                return err;
            }else{
                return Customer;
            }
        })
        .then(Customer => res.status(200).json(Customer))
        .catch(error => res.status(500).json({ 'error' : error }));
    }
};
const getAllCustomerName = (req, res) => {
    Customer.find({}, {'lastname':1, 'firstname':1, 'phoneNumber':1})
    .then(Customer => res.status(200).json(Customer))
    .catch(error => res.status(500).json({ 'error' : error }));
};
const getAllCustomerNameSelected = (req, res) => {
    var nameSelected = req.body.namePart;
    console.log('getAllCustomerNameSelected');
    console.log(nameSelected);

    userRegex = new RegExp(nameSelected, 'i')
    Customer.find({name: userRegex}, 'lastname firstname')
    .then(Customer => res.status(200).json(Customer))
    .catch(error => res.status(500).json({ 'error' : error }));
};
const getCustomer = (req, res) => {
    var id = req.body.id;
    console.log('getCustomer');
    console.log(id);
    Customer.findById(id).exec()
    .then(customer => res.status(200).json(customer))
    .catch(error => res.status(500).json({ 'error' : error }));
};
const setSaveInformationsQuote = (req, res) => {
    //déclarations variables
    var status = req.body.status;
    var id = req.body.cli_id;
    var civ = req.body.cli_civ;
    var name = req.body.cli_lastname;
    var firstname = req.body.cli_firstname;
    var address = req.body.cli_address;
    var compaddress = req.body.cli_compaddress;
    var cp = req.body.cli_cp;
    var city = req.body.cli_city;
    var phonenumber = req.body.cli_phonenumber;
    var mail = req.body.cli_mail;

    //controle si customer existe avant création
    if(status == "created"){
        //Si pas existant alors création du customer
        let newCustomer = new Customer({
            civility: civ, 
            lastname: name, 
            firstname: firstname, 
            address: address,
            compaddress: compaddress,
            postalcode: cp, 
            city: city, 
            phoneNumber: phonenumber,
            emailAddress: mail,
        });
        newCustomer.save();
        return res.status(200).json(newCustomer);
    } else if (status == "modify") {
        //Si existant on fait un update
        console.log('on est dans l\'update');
        Customer.findOneAndUpdate( {_id : id} , {
            civility: civ, lastname : name, firstname : firstname, 
            address: address, compaddress: compaddress, postalcode : cp,
            city : city, phoneNumber : phonenumber, emailAddress : mail,
        })
        .then(Customer => res.status(200).json(Customer))
        .catch(error => res.status(500).json({ 'error' : error }));
    }
};
const setSaveOptionsQuote = (req, res) => {
    console.log(req.body);
    //rechercher la dernière ref
    const researchLastReference = Project.find().sort({creationDate : -1}).limit(1);
    researchLastReference.exec(function(err,resultat){ 
        //Déclaration des variables
        var projectName = req.body.name;
        var constructaddress = req.body.cli_construct_address;
        var constructcompaddress = req.body.cli_construct_comp_address;
        var constructcp = req.body.cli_construct_cp;
        var constructcity = req.body.cli_construct_city;
        var projectcreationdate = req.body.cli_project_creation_date;
        var projectmodifydate = req.body.cli_project_modify_date;
        var idUser = req.body.user;
        var idCustomer = req.body.customer;
        var ref = "";
        if(err){
            ref = 1;
            console.log(ref);
        }
        else{
            ref = resultat[0].reference;
            console.log(ref);
        }
        var ref = parseInt(ref) + 1;
        console.log(ref);

        let newProject = new Project({
            reference: ref,
            name: projectName,
            state: "5f1024c37b23f59e1bc924bc",
            customer: idCustomer, 
            buildingAddress: constructaddress, 
            buildingCompAddress: constructcompaddress, 
            buildingPostalCode: constructcp, 
            buildingCity: constructcity,
            creationDate: projectcreationdate,
            modificationDate: projectmodifydate, 
            user: idUser, 
        });
        newProject.save();
        return res.status(200).json(newProject);
    });
};
const getResearchInformationsQuote = (req, res) => {
    //Déclaration des variables
    var ref = req.body.cli_ref;
    var phoneNumber = req.body.cli_phoneNumber;
    var name = req.body.cli_name;
    var email = req.body.cli_email;
    
    if (ref != "" || ref != NULL){
        Project.find({reference: ref}, (err, CustomerProjects) => {
            console.log(CustomerProjects);
            return CustomerProjects;
        }).populate('customer').populate('state')
        .then(CustomerProjects => res.status(200).json(CustomerProjects))
        .catch(error => res.status(500).json({ error }));
    }
    if (phoneNumber != "" && phoneNumber != NULL){
        Customer.findOne({phoneNumber: phoneNumber}, (err, OneCustomer) => {
            if (err){
                return err;
            } else {
                return OneCustomer;
            }
        })
        .then(OneCustomer => res.status(200).json(OneCustomer))
        .catch(err => res.status(500).json({ 'error' : err }));
    }
    if (name != "" && name != NULL){
        Customer.find({lastname: name}, (err, Customers) => {
            if (err){
                return err;
            } else {
                return Customers;
            }
        })
        .then(Customers => res.status(200).json(Customers))
        .catch(err => res.status(500).json({ 'error' : err }));
    }
    if (email != "" && email != NULL){
        Customer.findOne({emailAddress: email}, (err, OneCustomer) => {
            if (err){
                return err;
            } else {
                return OneCustomer;
            }
        })
        .then(OneCustomer => res.status(200).json(OneCustomer))
        .catch(err => res.status(500).json({ 'error' : err }));
    }
};
const getProjectProduct = (req,res) => {
    console.log('function getProjectProduct');
    //Déclaration des variables
    var project_id = req.body.idProject;

    ProjectProduct.find({projectid: project_id}, (err,refProduct) => {
        if (err){
            return err;
        } else {
            return refProduct.productid;
        }
    })
    .then(refProduct => res.status(200).json(refProduct))
    .catch(err => res.status(500).json({ 'error' : err }));
};
const getProduct = (req,res) => {
    console.log('function getProduct');
    //Déclaration des variables
    var product_id = req.body.idProduct;
    console.log(product_id);

    Product.find().where('_id').in(product_id).exec()
    .then(resProduct => res.status(200).json(resProduct))
    .catch(err => res.status(500).json({ 'error' : err }));
};
const getProjectModule = (req,res) => {
    console.log('function getProjectModule');
    //Déclaration des variables
    var project_Id = req.body.idProject;

    ProjectModule.find({projectid: project_Id}, (err,refModule) => {
        if (err){
            return err;
        } else {
            return refModule.moduleid;
        }
    })
    .then(refModule => res.status(200).json(refModule))
    .catch(err => res.status(500).json({ 'error' : err }));
};
const getModule = (req,res) => {
    console.log('function getModule');
    //Déclaration des variables
    var module_id = req.body.idModule;

    Module.find().where('_id').in(module_id).exec()
    .then(resModule => res.status(200).json(resModule))
    .catch(err => res.status(500).json({ 'error' : err }));
};
const getProjectComponent = (req,res) => {
    //Déclaration des variables
    var project_id = req.body.idProject;

    ProjectComponent.find({projectid: project_id}, (err,refComponent) => {
        if (err){
            return err;
        } else {
            return refComponent.component;
        }
    })
    .then(refComponent => res.status(200).json(refComponent))
    .catch(err => res.status(500).json({ 'error' : err }));
};
const getComponent = (req,res) => {
    console.log('function getComponent');
    //Déclaration des variables
    var components_id = req.body.idComponent;
    console.log('******************components_id****************');
    console.log(components_id);
    QuoteComponent.find({'_id': { '$in': components_id}}).populate('product').populate('module').exec()
    .then(resComponent => res.status(200).json(resComponent))
    .catch(err => res.status(500).json({ 'error' : err }));
};
const deleteQuote = (req,res) => {
    //Déclaration des variables
    var ref = req.body.reference;
    //il faut supprimer les component, product, module, projectModule, projectProduct et projectComponent
    Project.findOneAndDelete({reference: ref}, (err, ProjectDelete) => {
        return ProjectDelete
    })
    .then(ProjectDelete => res.status(200).json(ProjectDelete))
    .catch(err => res.status(500).json({ 'error' : 'erreur de suppression' }));
};
const setSaveOptionProduct = (req,res) => {
    console.log(req.body);
    //Déclaration des variables
    var productName = req.body.nameProduct;
    var productRange = req.body.selectedRange;
    var productModel = req.body.selectedModel;
    var productPrincipalCut = req.body.selectedPrincipalCut;
    var projectId = req.body.projectId;

    //création du produit
    let newProduct = new Product({
        name: productName,
        range: productRange,
        model: productModel,
        principalcut: productPrincipalCut, 
    });
    newProduct.save();


    var productId = newProduct._id;
    console.log("id du produit créé :");
    console.log(productId);

    let newProjectProduct = new ProjectProduct({
        projectid : projectId,
        productid : productId,
    })
    newProjectProduct.save();
    return res.status(200).json({'product' : newProduct, 'productproject' : newProjectProduct});
};
const setSaveOptionModule = (req,res) => {
    console.log(req.body);
    //Déclaration des variables
    var moduleName = req.body.name;
    var moduleLength = req.body.length;
    var moduleSection = req.body.selected_section;
    var moduleCorner = req.body.selected_corner;
    var projectId = req.body.projectId;

    //création d'un module
    let newModule = new Module({
        name: moduleName,
        length: moduleLength,
        section: moduleSection,
        corner: moduleCorner, 
    });
    newModule.save();
    var moduleId = newModule._id;
    console.log("id du module créé :");
    console.log(moduleId);

    let newProjectModule = new ProjectModule({
        projectid : projectId,
        moduleid : moduleId,
    })
    newProjectModule.save();
    return res.status(200).json({'module' : newModule, 'moduleproject' : newProjectModule});
};
const setSaveOptionComponent = (req,res) => {
    //Déclaration des variables
    var componentName = req.body.nameComponent;
    var componentProduct = req.body.selectedProduct;
    var componentModule = req.body.selectedModule;
    var projectId = req.body.projectId;

    let newComponent = new QuoteComponent({
        name: componentName,
        productid: componentProduct,
        moduleid: componentModule, 
    });
    newComponent.save();
    var componentId = newComponent._id;

    let newProjectComponent = new ProjectComponent({
        projectid : projectId,
        componentid : componentId,
    })
    newProjectComponent.save();
    return res.status(200).json({'component': newComponent, 'componentproject': newProjectComponent});
};

const createPdf = (req, res) => {
    var customer = req.body.customer;//, modules, products, components
    var project = req.body.project;
    var login = req.body.login;
    var components = req.body.components;
    var modules = req.body.modules;
    var products = req.body.products;
    var ranges = req.body.ranges;
    var models = req.body.models;

    let devis = new PDFDocument({ size: "A4", margin: 50 });

    generateHeader(devis);
    generateInformations(devis, customer, project, login, 1);
    generateConponentTable(devis, components);
    generateFooter(devis);

    devis.end();
    devis.pipe(fs.createWriteStream('C:\\Maisons modulaires\\Devis_'+ project.name + '_' + project.reference +'.pdf'));

    let dossierTechnique = new PDFDocument({ size: "A4", margin: 50 });

    generateHeader(dossierTechnique);
    generateInformations(dossierTechnique, customer, project, login, 2);
    generateProductModuleTable(dossierTechnique, modules, products, ranges, models);
    generateFooter(dossierTechnique);

    dossierTechnique.end();
    dossierTechnique.pipe(fs.createWriteStream('C:\\Maisons modulaires\\Dossier_Technique_'+ project.name + '_' + project.reference +'.pdf'));
};

function generateHeader(doc) {
    doc
        .image("logo.png", 50, 45, { width: 50 })
        .fillColor("#444444")
        .fontSize(20)
        .text("MADERA", 110, 57)
        .fontSize(10)
        .text("MADERA", 200, 50, { align: "right" })
        .text("123 rue de Madera", 200, 65, { align: "right" })
        .text("Lille, LL, 59000", 200, 80, { align: "right" })
        .moveDown();
}

function generateInformations(doc, customer, project, login, numDoc) {

    if(numDoc == 1){
        doc
            .fillColor("#444444")
            .fontSize(20)
            .text("Devis " + project.reference, 50, 160);
        
        generateHr(doc, 185);

        const customerInformationTop = 200;
        
        var dateFormat = require('dateformat');
        
        var civ ;
        
        if(customer.civility == 1)
        {
            civ = "Mr";
        }
        else if(customer.civility == 2){
            civ = "Mme";
        }
        else if(customer.civility == 3){
            civ = "Mlle";
        }
        
        doc
            .fontSize(10)
            .text("Référence client:", 50, customerInformationTop)
            .font("Helvetica-Bold")
            .text(project.customer._id, 150, customerInformationTop)
            .font("Helvetica-Bold")
            .text(civ + '. ' + customer.lastname + ' ' + customer.firstname, 50, customerInformationTop + 15)
            .font("Helvetica")
            .text(customer.city + ', '+ customer.postalcode + ', ' + customer.address, 50, customerInformationTop + 30)
            .font("Helvetica")
            .text(customer.phoneNumber, 300, customerInformationTop)
            .font("Helvetica")
            .text(customer.emailAddress, 300, customerInformationTop + 15)
            .moveDown();
        
        generateHr(doc, 252);
        
        const projectInformationTop = 267;
        
        doc
            .fontSize(10)
            .text("Référence projet :", 50, projectInformationTop)
            .font("Helvetica-Bold")
            .text(project.reference, 150, projectInformationTop)
            .font("Helvetica")
            .text("Date d'édition:", 50, projectInformationTop + 15)
            .text( dateFormat(project.creationDate, "dd/MM/yyyy"), 150, projectInformationTop + 15)
            .text("Prix:", 50, projectInformationTop + 30)
            .text("200 000 €", 150, projectInformationTop + 30)
            .font("Helvetica")
            .text("Vendeur :", 300, projectInformationTop)
            .text(login, 350, projectInformationTop)
            .moveDown();

        generateHr(doc, 319);
    }
    else if(numDoc == 2){
        doc
            .fillColor("#444444")
            .fontSize(20)
            .text("Dossier technique " + project.reference, 50, 160);

            generateHr(doc, 185);

        const projectInformationTop = 200;
        
        var dateFormat = require('dateformat');
        
        var civ ;
        
        if(customer.civility == 1)
        {
            civ = "Mr";
        }
        else if(customer.civility == 2){
            civ = "Mme";
        }
        else if(customer.civility == 3){
            civ = "Mlle";
        }
        
        doc
            .fontSize(10)
            .text("Référence projet :", 50, projectInformationTop)
            .font("Helvetica-Bold")
            .text(project.reference, 150, projectInformationTop)
            .font("Helvetica-Bold")
            .text(civ + '. ' + customer.lastname + ' ' + customer.firstname, 50, projectInformationTop + 15)
            .font("Helvetica")
            .text("Date d'édition:", 50, projectInformationTop + 30)
            .text( dateFormat(project.creationDate, "dd/MM/yyyy"), 150, projectInformationTop + 30)
            .font("Helvetica")
            .text("Vendeur :", 300, projectInformationTop)
            .text(login, 350, projectInformationTop)
            
            .moveDown();
        
        generateHr(doc, 252);
    }
}

function generateConponentTable(doc, components) {
        let i;
        const invoiceTableTop = 390;
        let amountTotal = 0;
        let quantityTotal = 0;
        let quantity = 0;
    
        doc.font("Helvetica-Bold");
        generateTableRowComponent(doc, invoiceTableTop, "Composents", "Produits", "Modules", "Quantités", "Prix total");
        generateHr(doc, invoiceTableTop + 20);
        doc.font("Helvetica");
    
        for (i = 0; i < components.length; i++) {
            const position = invoiceTableTop + (i + 1) * 30;
            quantity = Math.floor(Math.random() * (30 - 1 + 1) + 1)
            quantityTotal += quantity;
            amountTotal += components[i].amount;
            generateTableRowComponent(doc, position, components[i].name, components[i].product.name, components[i].module.name, quantity, components[i].amount);
            
            generateHr(doc, position + 20);
        }
    
        const subtotalPosition = invoiceTableTop + (i + 1) * 30;
        generateTableRowComponent(doc, subtotalPosition, "", "", "Total", quantityTotal, "152300");
}

function generateProductModuleTable(doc, modules, products, ranges, models) {

    let i, a;
    const invoiceTableTopProduct = 300;
    var tableLenght = invoiceTableTopProduct;
    var range = "";
    var model = "";


    doc.font("Helvetica-Bold");
    generateTableRowProducts(doc, invoiceTableTopProduct, "Produit", "Gamme", "Modèle", "CCTP");
    generateHr(doc, invoiceTableTopProduct + 20);
    tableLenght += 20;
    doc.font("Helvetica");

    for (i = 0; i < products.length; i++) {
        const position = invoiceTableTopProduct + (i + 1) * 30;

        for(a = 0; a < ranges.length; a++){
            if(products[i].range == ranges[a]._id)
            {
                range = ranges[a].name;
            }
        }

        for(a = 0; a < models.length; a++){
            if(products[i].model == models[a]._id)
            {
                model = models[a].nature;
            }
        }

        generateTableRowProducts(doc, position, products[i].name, range, model, products[i].principalcut);

        generateHr(doc, position + 20);
        tableLenght = position + 20;

    }

    ////////////////////////////////////////////////////////////////////////////////////
    const invoiceTableTopModule = tableLenght + 50;

    doc.font("Helvetica-Bold");
    generateTableRowModules(doc, invoiceTableTopModule, "Module", "Section", "Longeur (m)", "Angle");
    generateHr(doc, invoiceTableTopModule + 20);
    doc.font("Helvetica");

    for (i = 0; i < modules.length; i++) {
        const position = invoiceTableTopModule + (i + 1) * 30;
        generateTableRowModules(doc, position, modules[i].name, modules[i].section, modules[i].length, modules[i].corner);

        generateHr(doc, position + 20);
    }
}

function generateFooter(doc) {
    doc
        .fontSize(10)
        .text(
        "Payment is due within 15 days. Thank you for your business.",
        50,
        780,
        { align: "center", width: 500 }
        );
}

function generateTableRowComponent(doc, y, component, products, module, quantity, lineTotal) 
{
    doc
        .fontSize(10)
        .text(component, 50, y)
        .text(products, 140, y, { width: 90, align: "right" })
        .text(module, 270, y, { width: 90, align: "right" })
        .text(quantity, 380, y, { width: 90, align: "right" })
        .text(lineTotal + " €", 0, y, { align: "right" });
}
function generateTableRowProducts(doc, y, products, range, model, cctp) 
{
    var cctpText;
    if(cctp == 1){
        cctpText = "Horizontale";
    }
    else if(cctp == 2){
        cctpText = "Verticale";
    }
    else{
        cctpText = cctp;
    }

    doc
        .fontSize(10)
        .text(products, 50, y)
        .text(range, 160, y, { width: 90, align: "right" })
        .text(model, 300, y, { width: 90, align: "right" })
        .text(cctpText, 0, y, {align: "right" })
}
function generateTableRowModules(doc, y, module, section, lenght, corner) 
{
    var sectionText;
    if(section == 1){
        sectionText = "Section droite";
    }
    else{
        sectionText = section;
    }

    var cornerText;
    if(corner == 1){
        cornerText = "Angle droit";
    }
    else if(corner == 2){
        cornerText = "Angle interne";
    }
    else if(corner == 3){
        cornerText = "Angle externe";
    }
    else{
        cornerText = corner;
    }

    doc
        .fontSize(10)
        .text(module, 50, y)
        .text(sectionText, 160, y, { width: 90, align: "right" })
        .text(lenght, 300, y, { width: 90, align: "right" })
        .text(cornerText, 0, y, {align: "right" })
}

function generateHr(doc, y) {
    doc
        .strokeColor("#aaaaaa")
        .lineWidth(1)
        .moveTo(50, y)
        .lineTo(550, y)
        .stroke();
}

module.exports = {
    getAllUserQuotesList : getAllUserQuotesList,
    getAllUserProgressQuotesList : getAllUserProgressQuotesList,
    getAllUserValidateQuotesList : getAllUserValidateQuotesList,
    getAllUserQuotesListByPhoneNumber : getAllUserQuotesListByPhoneNumber,
    setSaveInformationsQuote : setSaveInformationsQuote,
    setSaveOptionsQuote : setSaveOptionsQuote,
    setSaveOptionProduct : setSaveOptionProduct,
    setSaveOptionModule : setSaveOptionModule,
    setSaveOptionComponent : setSaveOptionComponent,
    getResearchInformationsQuote : getResearchInformationsQuote,
    getAllCustomerName : getAllCustomerName,
    getAllCustomerNameSelected : getAllCustomerNameSelected,
    getResearchCustomer : getResearchCustomer,
    getCustomer : getCustomer,
    getProjectProduct : getProjectProduct,
    getProduct : getProduct,
    getProjectModule : getProjectModule,
    getModule : getModule,
    getProjectComponent : getProjectComponent,
    getComponent : getComponent,
    deleteQuote : deleteQuote ,
    createPdf : createPdf ,
}