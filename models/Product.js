const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    model: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Model"
    },
    range: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Range"
    },
    principalcut: {
        type: String,
        required: true
    },
});

module.exports = mongoose.model('Product', productSchema);