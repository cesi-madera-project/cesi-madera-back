const mongoose = require('mongoose');

const rangeSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    exteriorFinish: {
        type: String,
        required: true
    },
    insulationType: {
        type: String,
        required: true
    },
    coverType: {
        type: String,
        required: true
    },
    angleFrame: {
        type: String,
        required: true
    },
    frame: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Frame"
    },
});

module.exports = mongoose.model('Range', rangeSchema);