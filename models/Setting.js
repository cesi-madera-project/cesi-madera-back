const mongoose = require('mongoose');

const settingSchema = mongoose.Schema({
    tradingMargin: {
        type: String,
        required: true
    },
    businessMargin: {
        type: String,
        required: true
    },
    VAT: {
        type: String,
        required: true
    },
});

module.exports = mongoose.model('Setting', settingSchema);