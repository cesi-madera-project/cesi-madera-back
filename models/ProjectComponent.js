const mongoose = require('mongoose');

const projectcomponentSchema = mongoose.Schema({
    componentid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "QuoteComponent"
    },
    projectid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Project"
    },
});

module.exports = mongoose.model('ProjectComponent', projectcomponentSchema);