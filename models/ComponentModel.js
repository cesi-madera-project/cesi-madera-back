const mongoose = require('mongoose');

const componentmodelSchema = mongoose.Schema({
    model: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Model"
    },
    component: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Component"
    },
    amount: {
        type: String,
        required: true
    },
});


module.exports = mongoose.model('ComponentModel', componentmodelSchema);