const mongoose = require('mongoose');

const projectmoduleSchema = mongoose.Schema({
    projectid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Project"
    },
    moduleid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Module"
    },
});

module.exports = mongoose.model('ProjectModule', projectmoduleSchema);