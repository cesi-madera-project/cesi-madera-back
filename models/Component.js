const mongoose = require('mongoose');

const componentSchema = mongoose.Schema({
    nature: {
        type: String,
        required: true
    },
    specification: {
        type: String,
        required: true
    },
    unitOfUse: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    provider: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Provider"
    },
    familyComponent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "FamilyComponent"
    },
});

module.exports = mongoose.model('Component', componentSchema);