const mongoose = require('mongoose');
const Customer = require('./Customer');
const Payment = require('./Payment');
const State = require('./State');
const User = require('./User');

const projectSchema = mongoose.Schema({
  reference: {type: String },
  name: { type: String },
  state: { type: mongoose.Schema.Types.ObjectId, ref: "State" },
  customer: { type: mongoose.Schema.Types.ObjectId, ref: "Customer" },
  buildingAddress: { type: String, required: true },
  buildingCompAddress: { type: String },
  buildingPostalCode: { type: String, required: true },
  buildingCity: { type: String, required: true },
  frame: { type: String },
  amount: { type: Number },
  creationDate: { type: String },
  modificationDate: { type: String, required: true },
  payment: { type: mongoose.Schema.Types.ObjectId, ref: "Payment" },
  user: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
});

module.exports = mongoose.model('projects', projectSchema);