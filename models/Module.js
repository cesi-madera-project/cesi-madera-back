const mongoose = require('mongoose');

const moduleSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    length: {
        type: String,
        required: true
    },
    section: {
        type: String,
        required: true
    },
    corner: {
        type:String,
        required: true
    }
});

module.exports = mongoose.model('Module', moduleSchema);