const mongoose = require('mongoose');

const familycomponentSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
});

module.exports = mongoose.model('FamilyComponent', familycomponentSchema);