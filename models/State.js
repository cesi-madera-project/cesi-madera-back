const mongoose = require('mongoose');

const stateSchema = mongoose.Schema({
    wording: {
        type: String,
        required: true
    },
});

module.exports = mongoose.model('State', stateSchema);