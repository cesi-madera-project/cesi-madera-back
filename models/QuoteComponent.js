const mongoose = require('mongoose');

const quotecomponentSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product"
    },
    module: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Module"
    },
    amount: {
        type: String,
    },
});

module.exports = mongoose.model('QuoteComponent', quotecomponentSchema);