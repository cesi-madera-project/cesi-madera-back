const mongoose = require('mongoose');

const paymentSchema = mongoose.Schema({
    step: {
        type: String,
        required: true
    },
    percentage: {
        type: String,
        required: true
    },
});


module.exports = mongoose.model('Payment', paymentSchema);