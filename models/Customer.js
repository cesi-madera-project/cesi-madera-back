const mongoose = require('mongoose');

const customerSchema = mongoose.Schema({
    civility: { type: String, required: true },
    lastname: { type: String, required: true },
    firstname: { type: String, required: true },
    address: { type: String, required: true },
    compaddress: { type: String },
    postalcode: { type: String, required: true },
    city: { type: String, required: true },
    phoneNumber: { type: String, required: true },
    emailAddress: { type: String, required: true },
});

module.exports = mongoose.model('Customer', customerSchema);