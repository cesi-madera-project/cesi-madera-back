const mongoose = require('mongoose');

const projectproductSchema = mongoose.Schema({
    projectid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Project"
    },
    productid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product"
    },
});

module.exports = mongoose.model('ProjectProduct', projectproductSchema);