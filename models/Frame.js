const mongoose = require('mongoose');

const frameSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    quality: {
        type: String,
        required: true
    },
    pictureLink: {
        type: String,
        required: true
    },
});

module.exports = mongoose.model('Frame', frameSchema);