const mongoose = require('mongoose');

const modelSchema = mongoose.Schema({
    range: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Range",
    },
    nature: {
        type: String,
        required: true
    },
    specification: {
        type: String,
        required: true
    },
    unitOfUse: {
        type: String,
        required: true
    },
    filling: {
        type: String,
        required: true
    },
    principleCut: {
        type: String,
        required: true
    },
    specialTechnicalClauses: {
        type: String,
        required: true
    },
});

module.exports = mongoose.model('Model', modelSchema);
