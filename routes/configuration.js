const router = require('express').Router();

const configurationCtrl = require('../controllers/configuration');

router.get('/modele', configurationCtrl.getAllModel);
router.post('/modele', configurationCtrl.createModel);
router.get('/modele/:id', configurationCtrl.getOneModel);
router.put('/modele/:id', configurationCtrl.modifyModel);
router.delete('/modele/:id', configurationCtrl.deleteModel);

router.get('/gamme', configurationCtrl.getAllRange);
router.post('/gamme', configurationCtrl.createRange);
router.get('/gamme/:id', configurationCtrl.getOneRange);
router.put('/gamme/:id', configurationCtrl.modifyRange);
router.delete('/gamme/:id', configurationCtrl.deleteRange);

router.get('/famille-composant', configurationCtrl.getAllFamilyComponent);
router.post('/famille-composant', configurationCtrl.createFamilyComponent);
router.get('/famille-composant/:id', configurationCtrl.getOneFamilyComponent);
router.put('/famille-composant/:id', configurationCtrl.modifyFamilyComponent);
router.delete('/famille-composant/:id', configurationCtrl.deleteFamilyComponent);

router.get('/composant', configurationCtrl.getAllComponent);
router.post('/composant', configurationCtrl.createComponent);
router.get('/composant/:id', configurationCtrl.getOneComponent);
router.put('/composant/:id', configurationCtrl.modifyComponent);
router.delete('/composant/:id', configurationCtrl.deleteComponent);

router.get('/fournisseur', configurationCtrl.getAllProvider);
router.post('/fournisseur', configurationCtrl.createProvider);
router.get('/fournisseur/:id', configurationCtrl.getOneProvider);
router.put('/fournisseur/:id', configurationCtrl.modifyProvider);
router.delete('/fournisseur/:id', configurationCtrl.deleteProvider);

router.get('/huisserie', configurationCtrl.getAllFrame);
router.post('/huisserie', configurationCtrl.createFrame);
router.get('/huisserie/:id', configurationCtrl.getOneFrame);
router.put('/huisserie/:id', configurationCtrl.modifyFrame);
router.delete('/huisserie/:id', configurationCtrl.deleteFrame);

router.get('/parametres', configurationCtrl.getAllSetting);
router.post('/parametres', configurationCtrl.createSetting);
router.get('/parametres/:id', configurationCtrl.getOneSetting);
router.put('/parametres/:id', configurationCtrl.modifySetting);
router.delete('/parametres/:id', configurationCtrl.deleteSetting);

module.exports = router;