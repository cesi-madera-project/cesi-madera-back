const router = require('express').Router();
const connectionCtrl = require('../controllers/connection');

router.post('/', connectionCtrl.getLoginAcess);
//router.get('/user/:id', connectionCtrl.getAllUser);
//router.post('/', connectionCtrl.createUser);
//router.get('/:id', connectionCtrl.getOneUser);
//router.put('/:id', connectionCtrl.modifyUser);
//router.delete('/:id', connectionCtrl.deleteUser);

module.exports = router;
