const router = require('express').Router();

const quoteCtrl = require('../controllers/quote');

router.post('/all-quotes-user/', quoteCtrl.getAllUserQuotesList);
router.post('/progress-quotes-user/', quoteCtrl.getAllUserProgressQuotesList);
router.post('/validate-quotes-user/', quoteCtrl.getAllUserValidateQuotesList);
router.post('/research-project-by-phonenumber/', quoteCtrl.getAllUserQuotesListByPhoneNumber);
router.post('/save-informations-quote', quoteCtrl.setSaveInformationsQuote);
router.post('/save-options-quote', quoteCtrl.setSaveOptionsQuote);
router.post('/save-option-product', quoteCtrl.setSaveOptionProduct);
router.post('/save-option-module', quoteCtrl.setSaveOptionModule);
router.post('/save-option-component', quoteCtrl.setSaveOptionComponent);
router.post('/research-informations-quote', quoteCtrl.getResearchInformationsQuote);
router.post('/research-customer', quoteCtrl.getResearchCustomer);
router.post('/research-customer-name', quoteCtrl.getAllCustomerName);
router.post('/research-customer-name-selected', quoteCtrl.getAllCustomerNameSelected);
router.post('/get-customer', quoteCtrl.getCustomer);
router.post('/get-project-product', quoteCtrl.getProjectProduct);
router.post('/get-product', quoteCtrl.getProduct);
router.post('/get-project-module', quoteCtrl.getProjectModule);
router.post('/get-module', quoteCtrl.getModule);
router.post('/get-project-component', quoteCtrl.getProjectComponent);
router.post('/get-component', quoteCtrl.getComponent);
router.post('/delete-quote', quoteCtrl.deleteQuote);
router.post('/create-pdf', quoteCtrl.createPdf);

module.exports = router;