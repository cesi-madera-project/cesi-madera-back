const router = require('express').Router();

const paymentCtrl = require('../controllers/payment');

router.get('/projet-paiement', paymentCtrl.getAllProjectPayment);
router.post('/projet-paiement', paymentCtrl.createProjectPayment);
router.get('/projet-paiement/:id', paymentCtrl.getOneProjectPayment);
router.put('/projet-paiement/:id', paymentCtrl.modifyProjectPayment);
router.delete('/projet-paiement/:id', paymentCtrl.deleteProjectPayment);

router.get('/paiement', paymentCtrl.getAllPayment);
router.post('/paiement', paymentCtrl.createPayment);
router.get('/paiement/:id', paymentCtrl.getOnePayment);
router.put('/paiement/:id', paymentCtrl.modifyPayment);
router.delete('/paiement/:id', paymentCtrl.deletePayment);

module.exports = router;