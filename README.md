# cesi-madera-back

The back end of the madera app, it's a quote design app. this application is requested by the **CESI School**

## The link of the app (front & back)
* [FRONT END](https://gitlab.com/cesi-madera-livrable/cesi-madera-front)
* [BACK OFFICE](https://gitlab.com/cesi-madera-livrable/cesi-madera-back)

## Requirements

* nodeJS
* express
* nodemon

## Getting started 

- $ git clone https://gitlab.com/cesi-madera-livrable/cesi-madera-back.git # https clone
- $ cd cesi-madera-back
- $ npm install
- $ nodemon server