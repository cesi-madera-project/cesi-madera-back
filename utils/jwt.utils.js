var jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = "ZGV2d2lubmVyTUFERVJB";

module.exports = {
    generateTokenForUser: function (userData) {
        return jwt.sign({
            userID: userData.id
        }, JWT_SIGN_SECRET, { expiresIn: '1h' })
    }
}