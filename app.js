const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require("mongoose");
const cors = require('cors');
const cookieParser = require('cookie-parser');
const session = require('express-session');

//accès des modèles
const quoteRoutes = require('./routes/quote');
const paymentRoutes = require('./routes/payment');
const configurationRoutes = require('./routes/configuration');
const connexionRoutes = require('./routes/connection');

const app = express();
//connexion à la base mongoDB de développement
mongoose.connect(
    "mongodb+srv://Adrien:mongoDB2020@cluster1.0wfuk.mongodb.net/MADERAmodularHouse?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({ secret: "madera", cookie: { maxAge: 600000 } }));

app.use(cors());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.get('/', (req, res) => {
    res.write('<html>');
    res.write('<body>');
    res.write('<h1 style=\'text-align: center;\'>Vous etes à la racine de l\'API de MADERA, rendez-vous à l\'adresse /api pour plus de détails</h1>');
    res.write('</body>');
    res.write('</html>');
    res.end();
});

app.get('/api', (req, res) => {
    res.write('<html>');
    res.write('<body>');
    res.write('<h1 style=\'text-align: center;\'>API de l\'application MADERA</h1>');

    res.write('<h2>Chemin : </h2>');
    res.write('<ul>');
    res.write('<li>/api/quote</li>');
    res.write('<li>/api/payment</li>');
    res.write('<li>/api/configuration</li>');
    res.write('<li>/api/connexion</li>');
    res.write('<ul>');

    res.write('</body>');
    res.write('</html>');
    res.end();
});

app.use('/api/quote', quoteRoutes);

app.use('/api/payment', paymentRoutes);

app.use('/api/configuration', configurationRoutes);

app.use('/api/connexion', connexionRoutes);

module.exports = app;
