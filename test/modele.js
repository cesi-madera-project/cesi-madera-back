process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Model = require('../models/Model.js');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server.js');
let should = chai.should();

chai.use(chaiHttp);

describe('Model', () => {
    beforeEach((done) => {
		Model.remove({}, (err) => {
			done();
		});
    });
    /*
    * Test the /GET route
    */
    describe('/GET modele', () => {
		it('it should GET all the models', (done) => {
			chai.request("http://localhost:3000")
				.get('/api/configuration/modele')
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('array')
					res.body.length.should.be.eql(0);
					done();
				});
		});
    });
	/*
	 * Test the /POST route
	*/
	describe('/POST modele', () => {
		it('it should not POST a model without nature', (done) => {
			let model = {
				specification: "test",
				unitOfUse: "test",
				filling: "test",
				principleCut: "test",
				specialTechnicalClauses: "test"
			}
			chai.request("http://localhost:3000")
				.post('/api/configuration/modele')
				.send(model)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('errors');
					res.body.errors.should.have.property('nature');
					res.body.errors.nature.should.have.property('kind').eql('required');
				done();
				});
				
		}); 
		it('it should POST a model', (done) => {
			let model = {
				nature: "test",
				specification: "test",
				unitOfUse: "test",
				filling: "test",
				principleCut: "test",
				specialTechnicalClauses: "test"
			}
			chai.request("http://localhost:3000")
				.post('/api/configuration/modele')
				.send(model)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('Object');
					res.body.should.have.property('message').eql('modele created!');
					res.body.model.should.have.property('nature');
					res.body.model.should.have.property('specification');
					res.body.model.should.have.property('unitOfUse');
					res.body.model.should.have.property('filling');
					res.body.model.should.have.property('principleCut');
					res.body.model.should.have.property('specialTechnicalClauses');
				done();
				});
				
		}); 
	});
	setTimeout(function() { process.exit(); }, 3000);
});
